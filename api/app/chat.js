const {nanoid} = require("nanoid");
const Message = require("../models/Message");
const User = require("../models/User");
const activeConnections = {};

module.exports = async (ws, req) => {

    const id = nanoid();
    console.log('client connected! id=', id);
    activeConnections[id] = ws;
    let user = null;

    const messagesDB = await Message.find({}, null, {limit: 10, sort: {'_id': -1}})
    messagesDB.reverse();

    ws.send(JSON.stringify({
        type: 'PREV_MESSAGES',
        messages: messagesDB,
    }));

    ws.on('message', async (msg) => {
        const decodedMessage = JSON.parse(msg);
        switch (decodedMessage.type) {
            case 'LOGIN':
                const token = decodedMessage.token
                user = await User.findOne({token: token});
                user.status = 'online'
                await user.save();
                const users = await User.find({status: 'online'});
                console.log(users)
                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId];
                    conn.send(JSON.stringify({
                        type: 'NEW_USER',
                        users: users
                    }));
                });
                break;

            case 'SEND_MESSAGE':
                if (user === null) break;
                const messageData = {
                    user: user._id,
                    text: decodedMessage.text,
                    userName: user.name
                };
                const userMessage = await new Message(messageData);
                userMessage.createTime();
                userMessage.save();
                Object.keys(activeConnections).forEach(connId => {
                    const conn = activeConnections[connId];
                    conn.send(JSON.stringify({
                        type: 'NEW_MESSAGE',
                        message: userMessage,
                    }));
                });
                break;
            default:
                console.log('Unknown message type:', decodedMessage.type);
        }
    });

    ws.on('close', async () => {
        user.status = 'offline'
        await user.save();
        Object.keys(activeConnections).forEach(connId => {
            const conn = activeConnections[connId];
            conn.send(JSON.stringify({
                type: 'DISCONNECT_USER',
                userID: user._id
            }));
        });
        console.log('client disconnected id=', id);
        delete activeConnections[id];
    })
}