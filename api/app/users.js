const express = require('express');
const mongoose = require("mongoose");
const User = require("../models/User");

const router = express.Router();

router.post('/', async (req, res, next) => {
    try {
        const userData = {
            email: req.body.email,
            password: req.body.password,
            name: req.body.name
        };
        const user = new User(userData);
        user.generateToken();
        await user.save();
        return res.send({message: 'Created user ', user});
    } catch (e) {
        if (e instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(e);
        }
        return next(e);
    }
});

router.post('/sessions', async (req, res, next) => {
    try {
        const user = await User.findOne({email: req.body.email});
        if (!user) {
            return res.status(400).send({message: 'User is not found'})
        }
        const isMatch = await user.checkPassword(req.body.password)
        if (!isMatch) {
            return res.status(400).send('Password is incorrect');
        }
        user.generateToken();
        await user.save();
        return res.send(user);

    } catch (e) {
        next(e);
    }
});

router.delete('/sessions', async (req, res, next) => {
    try {
        const token = req.get('Authorization');
        const message = {message: 'OK'};

        if (!token) return res.send(message);

        const user = await User.findOne({token});

        if (!user) return res.send(message);

        user.generateToken();
        await user.save();

        return res.send(message);
    } catch (e) {
        next(e);
    }
});



module.exports = router;