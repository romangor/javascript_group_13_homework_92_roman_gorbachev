import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { RegisterError } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { registerUserRequest } from '../../store/users.actions';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.sass']
})
export class RegistrationPageComponent implements AfterViewInit, OnDestroy{
  @ViewChild('f') form!: NgForm;

  loading: Observable<boolean>;
  error: Observable<null | RegisterError>;
  errorSub!: Subscription;

  constructor(private store: Store<AppState>) {
    this.error = store.select(state => state.users.registerError);
    this.loading = store.select(state => state.users.registerLoading);
  }

  ngAfterViewInit(): void {
    this.errorSub = this.error.subscribe(error => {
      if (error) {
        const message = error.errors.email.message;
        this.form.form.get('email')?.setErrors({serverError: message});
      } else {
        this.form.form.get('email')?.setErrors({});
      }
    });
  }

  onSubmit() {
    const userData = {
      email: this.form.value.email,
      password: this.form.value.password,
      name: this.form.value.name,
    };
    this.store.dispatch(registerUserRequest({user: userData}));
  }

  ngOnDestroy() {
    this.errorSub.unsubscribe();
  }
}
