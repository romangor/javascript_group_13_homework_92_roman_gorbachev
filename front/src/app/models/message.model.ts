import { onlineUser } from "./user.model";

export class Message {
  constructor(
    public userName: string,
    public text: string,
    public time: string,
  ) {}

}

export interface ServerMessage {
  type: string,
  userID: string
  message: Message,
  messages: Message[],
  users: onlineUser[];
}

