const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    text: {
        type: String
    },
    time: {
        type: String
    },
    userName: {
        type: String
    }
});

MessageSchema.methods.createTime = function () {
    const hours = new Date().getHours().toString();
    const minutes = new Date().getMinutes().toString();
    this.time = `${hours}:${minutes}`
};


const Message = mongoose.model('Message', MessageSchema);


module.exports = Message;