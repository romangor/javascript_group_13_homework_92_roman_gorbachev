import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UsersService } from '../services/users.service';
import {
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess, logoutUser, logoutUserRequest,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess
} from './users.actions';
import { mergeMap, tap } from 'rxjs';
import { of } from 'rxjs';
import { catchError } from 'rxjs';
import { map } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HelpersService } from '../services/helper.sevice';


@Injectable()
export class UsersEffects {
  constructor(
    private actions: Actions,
    private usersService: UsersService,
    private router: Router,
    private snackbar: MatSnackBar,
    private helpers: HelpersService,
  ){}

  registrationUser = createEffect(() => this.actions.pipe(
    ofType(registerUserRequest),
    mergeMap(({user}) => this.usersService.registrationUser(user).pipe(
      map((user) => registerUserSuccess({user})),
      tap(() =>{
        void this.router.navigate((['/chat']));
        this.snackbar.open('Register successful', "Ok", {duration: 3000});
      }),
      catchError(reqErr => {
        let registerError = null;
        if (reqErr instanceof HttpErrorResponse && reqErr.status === 400 ) {
          registerError = reqErr.error;
          this.snackbar.open('This user already exists', "OK", {duration: 3000})
        } else {
          this.snackbar.open('Server error', "OK", {duration: 3000})
        }
        return of(registerUserFailure( {error: registerError}));
      })
    ))
  ))

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({userData}) => this.usersService.login(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Login successful');
        void this.router.navigate(['/chat']);
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ))

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    mergeMap(() => {
      return this.usersService.logout().pipe(
        map(() => logoutUser()),
        tap(() => {
          this.helpers.openSnackbar('Logout successful');
          void this.router.navigate(['/']);
        })
      );
    }))
  )
}
