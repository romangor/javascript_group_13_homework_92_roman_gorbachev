export interface User {
  _id: string,
  email: string,
  token: string,
  name: string,
}

export interface onlineUser {
  _id: string,
  email: string,
  name: string,
}

export interface RegisterUserData {
  email: string,
  password: string,
  name: string,
}

export interface FieldError {
  message: string
}

export interface RegisterError {
  errors: {
    password: FieldError,
    email: FieldError
  }
}

export interface LoginUserData {
  email: string,
  password: string,
}

export interface LoginError {
  error: string
}

