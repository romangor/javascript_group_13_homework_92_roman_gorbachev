import { LoginError, RegisterError, User } from '../models/user.model';

export type UsersState = {
  user: User | null,
  registerLoading: boolean,
  registerError: RegisterError | null,
  loginLoading: boolean,
  loginError: null | LoginError,
}


export type AppState = {
  users: UsersState
}
