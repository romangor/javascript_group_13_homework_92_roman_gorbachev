import { AfterViewInit, Component } from '@angular/core';
import { Message, ServerMessage } from '../../models/message.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable } from 'rxjs';
import { onlineUser, User } from '../../models/user.model';

@Component({
  selector: 'app-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.sass']
})
export class ChatPageComponent implements AfterViewInit {
  userData!: User | null;
  user: Observable<null | User>;
  ws!: WebSocket;
  messages: Message[] = [];
  messageText!: string;
  users: onlineUser[] = [];

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
  }

  ngAfterViewInit(): void {
    this.user.subscribe(user => {
      this.userData = user
    });

    const run = () => {
      this.ws = new WebSocket('ws://localhost:8000/chat');

      setTimeout(() => this.ws.send(JSON.stringify({type: 'LOGIN', token: this.userData?.token})), 100);

      this.ws.onmessage = event => {

        const decoded: ServerMessage = JSON.parse(event.data);

        if (decoded.type === 'PREV_MESSAGES') {
          decoded.messages.forEach(message => {
            this.messages.push(message);
          })
        }

        if (decoded.type === 'NEW_MESSAGE') {
          const newMessage = new Message(decoded.message.userName, decoded.message.text, decoded.message.time)
          this.messages.push(newMessage);
        }

        if (decoded.type === 'NEW_USER') {
          this.users = [];
          decoded.users.forEach(user => {
            const newUser = {
              _id: user._id,
              email: user.email,
              name: user.name,
            }
            this.users.push(newUser);
          });
        }

        if (decoded.type === 'DISCONNECT_USER') {
          this.users.forEach((user, i) => {
            if (user._id === decoded.userID) {
              this.users.splice(1, i);
            }
          });

        }
      }

      this.ws.onclose = event => {
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', event.reason);
        setTimeout(function () {
          run();
        }, 1000);
      }

      this.ws.onerror = error => {
        console.error('Socket encountered error: ', error, 'Closing socket');
        this.ws.close();
      };
    }
    run();

  }

  sendMessage() {
    this.ws.send(JSON.stringify({
      type: 'SEND_MESSAGE',
      text: this.messageText,
      user: this.userData?._id
    }));
  }
}
