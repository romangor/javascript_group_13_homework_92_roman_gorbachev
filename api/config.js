module.exports = {
    mongo: {
        db: 'mongodb://localhost/chat-socket',
        options: {useNewUrlParser: true},
    }
};